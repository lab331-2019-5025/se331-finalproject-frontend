import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student/student-service';
import { ActivityService } from './service/activity/activity-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StudentsFileImplService } from './service/student/students-file-impl.service';
import { StudentsAddComponent } from './students/add/students.add.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatNativeDateModule, MatProgressBarModule, MatSelectModule
} from '@angular/material';
import { MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';
import { WaitingListComponent } from './teacher/waiting-list/waiting-list.component';
import { TeacherRoutingModule } from './teacher/teacher-routing.module';
import { ActivityRoutingModule } from './activity/activity-routing.module';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { ConfirmRejectStudentRegisteredComponent } from './admin/confirm-reject-student-registered/student-list.component';
import { ActivityFileDataImplService } from './service/activity/activity-file-data-impl.service';
import { ViewWaitingStudentComponent } from './teacher/view-student-list/view-student-list.component';
import { TeacherOwnActivityListComponent } from './teacher/ownActivityList/activity-list.component';
import { TeacherFileDataImplService } from './service/teacher/teacher-file-data-impl.service';
import { TeacherService } from './service/teacher/teacher-service';
import { UpdateComponent } from './activity/update/update.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { DialogActivityDataComponent } from './activity/list/dialog-activity-data/dialog-activity-data.component';
import { ActivityListComponent } from './activity/list/activity-list.component';
import { ViewEnrolledActivitiesComponent } from './students/view-enrolled-activities/view-enrolled-activities.component';
import { AddActivityComponent } from './activity/add/add.component';
import { LoginComponent } from './login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommentActivityComponent } from './activity/comment/comment.component';
import { ViewInfoComponent } from './students/view-info/view-info.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatFileUploadModule } from "mat-file-upload";
import { ViewEnrolledStudentComponent } from './teacher/view-enrolled-student/view-enrolled-student.component';
import { AdminService } from './service/admin/admin-service';
import { CommentService } from './service/comment/comment-service';
import { ActivityRestImplService } from './service/activity/activity-rest-impl.service';
import { AdminRestImplService } from './service/admin/admin-rest-impl.service';
import { CommentRestImplService } from './service/comment/comment-rest-impl.service';
import { StudentRestImplService } from './service/student/student-rest-impl.service';
import { TeacherRestImplService } from './service/teacher/teacher-rest-impl.service';
import { JwtInterceptorService } from './helpers/jwt-interceptor.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    WaitingListComponent,
    StudentsAddComponent,
    MyNavComponent,
    FileNotFoundComponent,
    LoginComponent,
    ViewWaitingStudentComponent,
    TeacherOwnActivityListComponent,
    UpdateComponent,
    UpdateProfileComponent,
    DialogActivityDataComponent,
    ViewEnrolledActivitiesComponent,
    ConfirmRejectStudentRegisteredComponent,
    ActivityListComponent,
    AddActivityComponent,
    CommentActivityComponent,
    ViewInfoComponent,
    ViewEnrolledStudentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFileUploadModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    StudentRoutingModule,
    TeacherRoutingModule,
    ActivityRoutingModule,
    AdminRoutingModule,
    MatDialogModule,
    AppRoutingModule,
    MatFileUploadModule,
    MatProgressBarModule,
    MatSelectModule ,
    MatSnackBarModule
  ],
  providers: [
    { provide: ActivityService, useClass: ActivityRestImplService },
    { provide: AdminService, useClass: AdminRestImplService },
    { provide: CommentService, useClass: CommentRestImplService },
    { provide: StudentService, useClass: StudentRestImplService },
    { provide: TeacherService, useClass: TeacherRestImplService },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogActivityDataComponent]
})

export class AppModule { }
