import Activity from './activity';
import Person from './person';

export default class Comment {
    id: number;
    message: string;
    timestamp: string;
    image: string;
    activity: Activity;
    author: Person;
}
