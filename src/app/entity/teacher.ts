import Activity from './activity'
import Comment from './comment';
import Person from './person';

export default class Teacher extends Person {
    activities: Activity[];
    comments: Comment[];
}
