import Person from './person';
import Activity from './activity';
import Comment from './comment';

export default class Student extends Person {
  studentId: string;
  dob: string;
  major: string;
  year: number;
  email: string;
  confirmed: boolean; 
  enrolledActivities: Activity[];
  pendingActivities: Activity[];
  rejectActivities: Activity[];
  comments: Comment[];
}
