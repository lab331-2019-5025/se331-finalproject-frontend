import Teacher from './teacher';
import Student from './student';
import Comment from './comment';

export default class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    startRegisDate: string;
    endRegisDate: string;
    startActDate: string;
    endActDate: string;
    startTime: string;
    endTime: string;
    time: string;
    host: Teacher;
    studentsEnrolled: Student[];
    studentsPending: Student[];
    studentsReject: Student[];
    comments: Comment[];
}
