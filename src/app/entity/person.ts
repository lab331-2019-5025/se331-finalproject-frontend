export default class Person {
    id: number;
    name: string;
    surname: string;
    image: string;
}
