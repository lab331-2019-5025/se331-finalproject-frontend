export class UpdateProfileForm {
    appUserId: number;
    newPassword: string;
    image: string;
    role: string;
}
