export class AddStudentForm {
    name: string;
    surname: string;
    dob: string;
    studentId: string;
    major: string;
    year: string;
    email: string;
    image: string;
    username: string;
    password: string;
    confirmPassword: string;
}
