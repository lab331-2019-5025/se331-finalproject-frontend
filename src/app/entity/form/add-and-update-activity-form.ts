export class AddAndUpdateActivityForm {
    activityId: number;
    name: string;
    location: string;
    description: string;
    startRegisDate: string;
    endRegisDate: string;
    startActDate: string;
    endActDate: string;
    startTime: string;
    endTime: string;
    hostId: number;
}
