export class AddCommentForm {
    message: string;
    timestamp: string;
    image: string;
    userId: number;
    activityId: number;
}
