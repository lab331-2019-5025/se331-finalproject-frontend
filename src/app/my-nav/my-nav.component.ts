import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {
  defaultImageUrl = 'assets/images/login.png'

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  isShowMenu() {
    if (window.innerWidth < 850) {
      return true;
    } else {
      return false;
    }
  }
    
  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  isRegis() {
    if (window.location.pathname === "registration") {
      return true;
    } else {
      return false;
    }
  }

  get user() {
    return this.authService.getCurrentUser();
  }

  getRole(user) {
    var role = user.authorities[0].name.substring(5);
    return role.substring(0, 1) + role.substring(1).toLowerCase();
  }

  updateProfile() {
    this.router.navigate(['updateProfile']);
  }

  logout() {
    alert("logout");
    this.router.navigate(['/login']);
  }

}
