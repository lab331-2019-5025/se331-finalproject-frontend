import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivityTableDataSource } from './activity-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { MatDialog } from '@angular/material';
import { DialogActivityDataComponent } from './dialog-activity-data/dialog-activity-data.component';
import Activity from 'src/app/entity/activity';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';


@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})

export class ActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'host', 'periodRegis', 'date', 'time', 'location', 'btnCol'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  searchBy = "name";
  formDate = this.fb.group({
    startRegis: [''],
    endRegis: ['']
  });

  constructor(
    private activityService: ActivityService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.activityService.getActivities()
      .subscribe(activities => {
        this.activities = activities;
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = this.activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      });
  }

  ngAfterViewInit() { }

  /**
   * getDate()	Get the day as a number (1-31)
   * getMonth()	Get the month as a number (0-11)
   * getFullYear()	Get the year as a four digit number (yyyy)
   * 
   * toISOString() = 2019-11-17T17:00:00.000Z
   * new Date("2019-11-17T17:00:00.000Z") = Mon Nov 18 2019 00:00:00 GMT+0700 (Indochina Time)
   * 
   * 1: only name
   * 2: start and end
   * 3: only start
   * 4: only end
   */
  applyFilter(filterValue: string) {
    this.filter$.next("1" + filterValue.trim().toLowerCase());
  }

  applyFilterPeriod() {
    console.log("Search by period of regis !!!");
    if (this.formDate.value['startRegis'] != "" && this.formDate.value['endRegis'] != "") {
      var keyWord = this.formDate.value['startRegis'].toISOString() + "," + this.formDate.value['endRegis'].toISOString();
      this.filter$.next("2" + keyWord);
    } else if (this.formDate.value['startRegis'] != "" && this.formDate.value['endRegis'] == "") {
      this.filter$.next("3" + this.formDate.value['startRegis'].toISOString());
    } else if (this.formDate.value['startRegis'] == "" && this.formDate.value['endRegis'] != "") {
      this.filter$.next("4" + this.formDate.value['endRegis'].toISOString());
    }
  }

  openActivityDetail(activity: Activity) {
    const dialogRef = this.dialog.open(DialogActivityDataComponent, {
      data: activity
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result == true) {
        var enrollForm = this.fb.group({
          studentId: this.authService.getCurrentUser().id,
          activityId: activity.id
        });

        // sent enroll info to backend here
        this.activityService.enrollActivity(enrollForm.value)
          .subscribe(activity => {
            alert("Enrollment complete");
            this.ngOnInit();
          }, (error) => {
            alert('could not save value');
          });
      }
    });
  }

  isSearchBy(input: String) {
    if (input == this.searchBy) {
      return true;
    } else {
      return false;
    }
  }

  changeSearchBy(key) {
    this.applyFilter("");
    this.formDate = this.fb.group({
      startRegis: "",
      endRegis: ""
    });
    this.searchBy = key;
  }

  goToComment(id) {
    this.router.navigate(['/viewComment/' + id]);
  }

  clearDate(input) {
    if (this.formDate.value['startRegis'] == "" || this.formDate.value['endRegis'] == "") {
      this.applyFilter("");
      this.formDate = this.fb.group({
        startRegis: "",
        endRegis: ""
      });
    } else if (this.formDate.value['startRegis'] != "" && this.formDate.value['endRegis'] != "") {
      if (input == "start") {
        var date = this.formDate.value['endRegis'];
        this.formDate = this.fb.group({
          startRegis: "",
          endRegis: date
        });
      } else if (input == "end") {
        var date = this.formDate.value['startRegis'];
        this.formDate = this.fb.group({
          startRegis: date,
          endRegis: ""
        });
      }
      this.applyFilterPeriod();
    }
  }

  getDate(startDate, endDate) {
    if (startDate == endDate) {
      return this.getDateHelper(startDate);
    } else {
      return this.getDateHelper(startDate) + " - " + this.getDateHelper(endDate);;
    }
  }

  getDateHelper(date) {
    var d = new Date(date);
    return this.dateFormatHelper(d.getDate()) + "/" + this.dateFormatHelper(d.getMonth() + 1) + "/" + d.getFullYear();
  }

  dateFormatHelper(date: number) {
    if (date.toString().length == 1) {
      return "0" + date;
    } else {
      return date;
    }
  }

  isPending(activity: Activity) {
    var result = false;
    for (var i = 0; i < activity.studentsPending.length; i++) {
      if (activity.studentsPending[i].id == this.authService.getCurrentUser().id) {
        result = true;
        break;
      }
    }
    return result;
  }

  isEnrolled(activity: Activity) {
    var result = false;
    for (var i = 0; i < activity.studentsEnrolled.length; i++) {
      if (activity.studentsEnrolled[i].id == this.authService.getCurrentUser().id) {
        result = true;
        break;
      }
    }
    return result;
  }

  isEnrollPage() {
    if (window.location.pathname == "/enrollActivity") {
      return true;
    } else {
      return false;
    }
  }

  isActivityListPage() {
    if (window.location.pathname == "/activityList") {
      return true;
    } else {
      return false;
    }
  }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

}
