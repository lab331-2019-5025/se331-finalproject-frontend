import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActivityDataComponent } from './dialog-activity-data.component';

describe('DialogActivityDataComponent', () => {
  let component: DialogActivityDataComponent;
  let fixture: ComponentFixture<DialogActivityDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogActivityDataComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActivityDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
