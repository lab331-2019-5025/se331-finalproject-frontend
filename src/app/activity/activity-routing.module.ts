import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AddActivityComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { ActivityListComponent } from './list/activity-list.component';
import { CommentActivityComponent } from './comment/comment.component';
import { AdminAndTeacherAndStudentGuard } from '../guard/admin-and-teacher-and-student.guard';
import { AdminGuard } from '../guard/admin.guard';
import { StudentGuard } from '../guard/student.guard';
import { TeacherGuard } from '../guard/teacher.guard';

const ActivityRoutes: Routes = [
    { path: 'addActivity', component: AddActivityComponent, canActivate: [AdminGuard] },
    { path: 'viewComment/:actId', component: CommentActivityComponent, canActivate: [AdminAndTeacherAndStudentGuard] },
    { path: 'activityList', component: ActivityListComponent, canActivate: [AdminAndTeacherAndStudentGuard] },
    { path: 'enrollActivity', component: ActivityListComponent, canActivate: [StudentGuard] },
    { path: 'updateActivity/:actId', component: UpdateComponent, canActivate: [TeacherGuard] }

];

@NgModule({
    imports: [
        RouterModule.forRoot(ActivityRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class ActivityRoutingModule {

}
