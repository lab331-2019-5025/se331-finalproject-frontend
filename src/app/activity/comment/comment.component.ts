import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Activity from 'src/app/entity/activity';
import { HttpEventType, HttpEvent } from '@angular/common/http';
import Teacher from 'src/app/entity/teacher';
import Student from 'src/app/entity/student';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { CommentService } from 'src/app/service/comment/comment-service';
import { StudentService } from 'src/app/service/student/student-service';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { TeacherService } from 'src/app/service/teacher/teacher-service';
import { FileUploadService } from 'src/app/service/file-upload.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentActivityComponent implements OnInit {
  activity: Activity;
  comments: any;
  student: Student;
  teacher: Teacher;
  uploadEndPoint: string;
  uploadedUrl: string;
  progress: number;
  defaultImageUrl = 'assets/images/login.png'

  form = this.fb.group({
    message: [''],
    timestamp: [''],
    image: [''],
    userId: [''],
    activityId: ['']
  });

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private activityService: ActivityService,
    private commentService: CommentService, private studentService: StudentService, private authService: AuthenticationService,
    private teacherService: TeacherService,
    private fileUploadService: FileUploadService) { }

  ngOnInit() {
    this.uploadEndPoint = environment.uploadApi;
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivityById(params['actId'])
          .subscribe(activity => {
            this.activity = activity;
            this.comments = activity.comments;
            this.comments.reverse();
          });
        //call student by id
        if (this.authService.getCurrentUser().authorities[0].name == "ROLE_STUDENT") {
          this.studentService.getStudentById(this.authService.getCurrentUser().id)
            .subscribe(student => {
              this.student = student;
              console.log(this.student);
            });
        } else if (this.authService.getCurrentUser().authorities[0].name == "ROLE_TEACHER") {
          this.teacherService.getTeacherById(this.authService.getCurrentUser().id)
            .subscribe(teacher => {
              this.teacher = teacher;
              console.log(this.teacher);
            });
        }
      });
  }

  submit() {
    this.form.patchValue({
      timestamp: new Date().toISOString(),
      userId: this.user.id,
      activityId: this.activity.id
    });
    this.commentService.saveComment(this.form.value)
      .subscribe((form) => {
        alert("Comment added!");
        this.form = this.fb.group({
          message: [''],
          timestamp: [''],
          image: [''],
          userId: [''],
          activityId: ['']
        });
        this.ngOnInit();
      }, (error) => {
        alert('Cannot add Comment!');
      });
  }

  onUploadClicked(files?: FileList) {
    console.log(typeof (files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
        }
      });
  }

  onSelectedFilesChanged(files?: FileList) { }

  getDate(timestamp) {
    return this.getDateHelper(timestamp);
  }

  getDateHelper(date) {
    var d = new Date(date);
    return this.dateFormatHelper(d.getDate()) + "/" + this.dateFormatHelper(d.getMonth() + 1) + "/" + d.getFullYear();
  }

  dateFormatHelper(date: number) {
    if (date.toString().length == 1) {
      return "0" + date;
    } else {
      return date;
    }
  }

  deleteComment(id: number) {
    this.commentService.removeComment(id)
      .subscribe((comment) => {
        alert('Cannot delete comment!');
      }, (error) => {
        alert("Delete!");
        this.ngOnInit();
      });
  }

  get user() {
    return this.authService.getCurrentUser();
  }

  isFill() {
    if (this.form.value['message'] != "" || this.form.value['image'] != "") {
      return true;
    } else {
      return false;
    }
  }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

}
