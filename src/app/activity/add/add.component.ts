import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import Teacher from 'src/app/entity/teacher';
import { TeacherService } from 'src/app/service/teacher/teacher-service';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddActivityComponent implements OnInit {
  teachers: Teacher[];

  form = this.fb.group({
    id: [''],
    name: [null, Validators.required],
    location: [null, Validators.required],
    host: [null, Validators.required],
    startRegisDate: ['', Validators.required],
    endRegisDate: [null, Validators.required],
    startActDate: [null, Validators.required],
    endActDate: [null, Validators.required],
    startTime: [null, Validators.pattern('(1[0-2]|0?[1-9]).([0-9]{2}) (a.m.|p.m.)$')],
    endTime: [null, Validators.pattern('(1[0-2]|0?[1-9]).([0-9]{2}) (a.m.|p.m.)$')],
    description: [null, Validators.required]
  });

  constructor(private fb: FormBuilder,
    private teacherService: TeacherService,
    private activityService: ActivityService,
    private router: Router
  ) { }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Activity name is required' }
    ],
    'location': [
      { type: 'required', message: 'Location of activity is required' }
    ],
    'host': [
      { type: 'required', message: 'Host name of activity is required' }
    ],
    'startRegisDate': [
      { type: 'required', message: 'Start period of registration is required' }
    ],
    'endRegisDate': [
      { type: 'required', message: ' End period of registration is required' }
    ],
    'startActDate': [
      { type: 'required', message: ' Start date of the activity is required' }
    ],
    'endActDate': [
      { type: 'required', message: ' End date of the activity is required' }
    ],
    'startTime': [
      { type: 'required', message: ' Start time of the activity is required' },
      { type: 'pattern', message: 'the pattern must be HH.MM a.m./p.m.' }
    ],
    'endTime': [
      { type: 'required', message: ' End time of the activity is required' },
      { type: 'pattern', message: 'the pattern must be HH.MM a.m./p.m.' }
    ],
    'description': [
      { type: 'required', message: 'Description of activity is required' }
    ]
  };

  submit() {
    var addActForm = this.fb.group({
      activityId: "",
      name: this.form.value['name'],
      location: this.form.value['location'],
      description: this.form.value['description'],
      startRegisDate: this.form.value['startRegisDate'].toISOString(),
      endRegisDate: this.form.value['endRegisDate'].toISOString(),
      startActDate: this.form.value['startActDate'].toISOString(),
      endActDate: this.form.value['endActDate'].toISOString(),
      startTime: this.form.value['startTime'],
      endTime: this.form.value['endTime'],
      hostId: this.form.value['host'].id
    });
    console.log(addActForm.value);
    this.activityService.saveActivity(addActForm.value)
      .subscribe((activity) => {
        alert("Added!")
        console.log(activity);
        this.router.navigate(['/activityList']);
      }, (error) => {
        alert('could not save value');
      });
  }

  ngOnInit(): void {
    this.teacherService.getTeachers()
      .subscribe(teachers => {
        this.teachers = teachers;
      });
  }
}
