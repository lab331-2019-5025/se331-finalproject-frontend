import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity/activity-service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})

export class UpdateComponent implements OnInit {
  activity: Activity;

  form = this.fb.group({
    activityId: [''],
    name: ['', Validators.required],
    location: ['', Validators.required],
    description: ['', Validators.required],
    startRegisDate: ['', Validators.required],
    endRegisDate: ['', Validators.required],
    startActDate: ['', Validators.required],
    endActDate: ['', Validators.required],
    startTime: ['', Validators.pattern('(1[0-2]|0?[1-9]).([0-9]{2}) (a.m.|p.m.)$')],
    endTime: ['', Validators.pattern('(1[0-2]|0?[1-9]).([0-9]{2}) (a.m.|p.m.)$')],
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private activityService: ActivityService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivityById(+params['actId'])
          .subscribe((inputActivity: Activity) => {
            this.activity = inputActivity;
            this.form.patchValue({
              name: this.activity.name,
              location: this.activity.location,
              description: this.activity.description,
              startRegisDate: new Date(this.activity.startRegisDate),
              endRegisDate: new Date(this.activity.endRegisDate),
              startActDate: new Date(this.activity.startActDate),
              endActDate: new Date(this.activity.endActDate),
              startTime: this.activity.startTime,
              endTime: this.activity.endTime
            });
          });
      });

  }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Activity name is required' }
    ],
    'location': [
      { type: 'required', message: 'Location of activity is required' }
    ],
    'startRegisDate': [
      { type: 'required', message: 'Start date of registration is required' }
    ],
    'endRegisDate': [
      { type: 'required', message: ' End date of registration is required' }
    ],
    'startActDate': [
      { type: 'required', message: ' Start date of the activity is required' }
    ],
    'endActDate': [
      { type: 'required', message: ' End date of the activity is required' }
    ],
    'startTime': [
      { type: 'required', message: ' Start time of the activity is required' },
      { type: 'pattern', message: 'The pattern must be HH.MM a.m./p.m.' }
    ],
    'endTime': [
      { type: 'required', message: ' End time of the activity is required' },
      { type: 'pattern', message: 'The pattern must be HH.MM a.m./p.m.' }
    ],
    'description': [
      { type: 'required', message: 'Description of activity is required' }
    ]
  };

  update() {
    this.form.patchValue({
      activityId: this.activity.id,
      startRegisDate: this.form.value['startRegisDate'].toISOString(),
      endRegisDate: this.form.value['endRegisDate'].toISOString(),
      startActDate: this.form.value['startActDate'].toISOString(),
      endActDate: this.form.value['endActDate'].toISOString(),
    });

    this.activityService.updateActivity(this.form.value)
      .subscribe((activity) => {
        alert("updated!");
        this.router.navigate(['/ownActivityList']);
      }, (error) => {
        alert('could not update activity');
      });
  }

}
