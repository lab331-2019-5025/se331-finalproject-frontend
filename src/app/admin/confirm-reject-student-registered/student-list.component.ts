import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { StudentTableDataSource } from './student-table-datasource';
import { StudentService } from 'src/app/service/student/student-service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class ConfirmRejectStudentRegisteredComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['studentId', 'name', 'surname', 'major', 'year', 'btnCol'];
  students: Student[];
  actName: string;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute,
    private studentService: StudentService,
    private acivityService: ActivityService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.studentService.getStudents()
      .subscribe(students => {
        var studentList = [];
        for (var i = 0; i < students.length; i++) {
          if (students[i].confirmed == false) {
            studentList.push(students[i]);
          }
        };

        this.dataSource = new StudentTableDataSource();
        this.dataSource.data = studentList;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.students = studentList;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      });

  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  confirm(student: Student) {
    this.studentService.confirmRegisStudent(student.id)
      .subscribe((form) => {
        alert(student.studentId + " " + student.name + " confirmed registration!");
        this.ngOnInit();
      }, (error) => {
        alert('could not confirm registration!');
      });
  }

  reject(student: Student) {
    this.studentService.rejectRegisStudent(student.id)
      .subscribe((remove) => {
      }, (error) => {
        alert(student.studentId + " " + student.name + "rejected registration!");
        this.ngOnInit();
      });
  }

  goToStudentInfo(id) {
    this.router.navigate(["/viewStudentinfo/" + id])
  }

}
