import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ConfirmRejectStudentRegisteredComponent } from './confirm-reject-student-registered/student-list.component';
import { AdminGuard } from '../guard/admin.guard';

const AdminRoutes: Routes = [
    { path: 'confirm-reject-student-registed', component: ConfirmRejectStudentRegisteredComponent, canActivate: [AdminGuard] }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AdminRoutingModule {

}
