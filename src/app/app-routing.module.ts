import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { LoginComponent } from './login/login.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { AdminAndTeacherAndStudentGuard } from './guard/admin-and-teacher-and-student.guard';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/activityList',
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  { path: 'updateProfile', component: UpdateProfileComponent, canActivate: [AdminAndTeacherAndStudentGuard] },
  { path: '**', component: FileNotFoundComponent }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
