import { Observable } from 'rxjs';
import Student from 'src/app/entity/student';
import { AddStudentForm } from 'src/app/entity/form/add-student-form';

export abstract class StudentService {
     abstract getStudents(): Observable<Student[]>;
     abstract getStudentById(id: number): Observable<Student>;
     abstract saveStudent(data: AddStudentForm): Observable<Student>;
     abstract rejectRegisStudent(id: number): Observable<String>;
     abstract confirmRegisStudent(id: number): Observable<String>;
}
