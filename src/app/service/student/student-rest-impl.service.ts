import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { HttpClient } from '@angular/common/http';
import Student from 'src/app/entity/student';
import { Observable } from 'rxjs';
import { AddStudentForm } from 'src/app/entity/form/add-student-form';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentRestImplService extends StudentService {
  constructor(private http: HttpClient) {
    super();
  }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }

  getStudentById(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  saveStudent(data: AddStudentForm): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, data);
  }

  rejectRegisStudent(id: number): Observable<String> {
    return this.http.post<String>(environment.rejectRegisStudentApi + '/' + id, { "studentId": id });
  }

  confirmRegisStudent(id: number): Observable<String> {
    return this.http.post<String>(environment.confirmRegisStudentApi + '/' + id, { "studentId": id });
  }

}
