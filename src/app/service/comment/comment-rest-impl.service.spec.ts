import { TestBed } from '@angular/core/testing';

import { CommentRestImplService } from './comment-rest-impl.service';

describe('CommentRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommentRestImplService = TestBed.get(CommentRestImplService);
    expect(service).toBeTruthy();
  });
});
