import { Observable } from 'rxjs';
import Comment from 'src/app/entity/comment';
import { AddCommentForm } from 'src/app/entity/form/add-comment-form';

export abstract class CommentService {
    abstract getComments(): Observable<Comment[]>;
    abstract getCommentById(id: number): Observable<Comment>;
    abstract saveComment(data: AddCommentForm): Observable<Comment>;
    abstract removeComment(id: number): Observable<String>;
}
