import { Injectable } from '@angular/core';
import { CommentService } from './comment-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Comment from 'src/app/entity/comment';
import { AddCommentForm } from 'src/app/entity/form/add-comment-form';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentRestImplService extends CommentService {
 
  constructor(private http: HttpClient) {
    super();
  }

  getComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(environment.commentApi);
  }

  getCommentById(id: number): Observable<Comment> {
    return this.http.get<Comment>(environment.commentApi + '/' + id);
  }

  saveComment(data: AddCommentForm): Observable<Comment> {
    return this.http.post<Comment>(environment.commentApi, data);
  } 
  
  removeComment(id: number): Observable<String> {
    return this.http.post<String>(environment.removeCommentApi + '/' + id, { "id": id });
  }
}