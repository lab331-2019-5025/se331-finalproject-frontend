import { Observable } from '../../../../node_modules/rxjs';
import Activity from 'src/app/entity/activity';
import { AddAndUpdateActivityForm } from 'src/app/entity/form/add-and-update-activity-form';
import { EnrollActForm } from 'src/app/entity/form/enroll-act-form';

export abstract class ActivityService {
    abstract getActivities(): Observable<Activity[]>;
    abstract getActivityById(id: number): Observable<Activity>;
    abstract saveActivity(data: AddAndUpdateActivityForm): Observable<Activity>;
    abstract updateActivity(data: AddAndUpdateActivityForm): Observable<Activity>;
    abstract enrollActivity(data: EnrollActForm): Observable<Activity>;
    abstract confirmEnrollActivity(data: EnrollActForm): Observable<Activity>;
    abstract rejectEnrollActivity(data: EnrollActForm): Observable<Activity>;
}
