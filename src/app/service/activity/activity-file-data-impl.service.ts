import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivityService } from './activity-service';
import { Observable } from '../../../../node_modules/rxjs';
import Activity from 'src/app/entity/activity';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ActivityFileDataImplService extends ActivityService {
  constructor(private http: HttpClient) {
    super();
  }

  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>('assets/activity.json');
  }

  getActivityById(id: number): Observable<Activity> {
    return this.http.get<Activity[]>('assets/activity.json')
      .pipe(map(activities => {
        const output: Activity = (activities as Activity[]).find(activity => activity.id === +id);
        return output;
      }));
  }

  saveActivity(activity: any): Observable<Activity> {
    throw new Error("Method not implemented.");
  }

  updateActivity(activity: any): Observable<Activity> {
    throw new Error("Method not implemented.");
  }

  enrollActivity(activity: any): Observable<Activity> {
    throw new Error("Method not implemented.");
  }

  confirmEnrollActivity(activity: any): Observable<Activity> {
    throw new Error("Method not implemented.");
  }

  rejectEnrollActivity(activity: any): Observable<Activity> {
    throw new Error("Method not implemented.");
  }
}
