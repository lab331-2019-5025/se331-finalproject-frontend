import { Injectable } from '@angular/core';
import { ActivityService } from './activity-service';
import { HttpClient } from '@angular/common/http';
import Activity from 'src/app/entity/activity';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { AddAndUpdateActivityForm } from 'src/app/entity/form/add-and-update-activity-form';
import { EnrollActForm } from 'src/app/entity/form/enroll-act-form';

@Injectable({
  providedIn: 'root'
})
export class ActivityRestImplService extends ActivityService {
  constructor(private http: HttpClient) {
    super();
  }

  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(environment.activityApi);
  }

  getActivityById(id: number): Observable<Activity> {
    return this.http.get<Activity>(environment.activityApi + '/' + id);
  }

  saveActivity(data: AddAndUpdateActivityForm): Observable<Activity> {
    return this.http.post<Activity>(environment.activityApi, data);
  }

  updateActivity(data: AddAndUpdateActivityForm): Observable<Activity> {
    return this.http.post<Activity>(environment.updateActivityApi, data);
  }

  enrollActivity(data: EnrollActForm): Observable<Activity> {
    return this.http.post<Activity>(environment.enrollActivityApi, data);
  }

  confirmEnrollActivity(data: EnrollActForm): Observable<Activity> {
    return this.http.post<Activity>(environment.confirmEnrollActivityApi, data);
  }

  rejectEnrollActivity(data: EnrollActForm): Observable<Activity> {
    return this.http.post<Activity>(environment.rejectEnrollActivityApi, data);
  }

}
