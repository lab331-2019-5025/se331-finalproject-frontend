import { Injectable } from '@angular/core';
import { TeacherService } from './teacher-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Teacher from 'src/app/entity/teacher';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherRestImplService extends TeacherService {
  constructor(private http: HttpClient) {
    super();
  }

  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(environment.teacherApi);
  }

  getTeacherById(id: number): Observable<Teacher> {
    return this.http.get<Teacher>(environment.teacherApi + '/' + id);
  }

}
