import { TestBed } from '@angular/core/testing';

import { TeacherRestImplService } from './teacher-rest-impl.service';

describe('TeacherRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeacherRestImplService = TestBed.get(TeacherRestImplService);
    expect(service).toBeTruthy();
  });
});
