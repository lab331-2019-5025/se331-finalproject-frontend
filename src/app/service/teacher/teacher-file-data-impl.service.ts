import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../../node_modules/rxjs';
import { map } from 'rxjs/operators';
import { TeacherService } from './teacher-service';
import Teacher from 'src/app/entity/teacher';

@Injectable({
  providedIn: 'root'
})

export class TeacherFileDataImplService extends TeacherService {
  constructor(private http: HttpClient) {
    super();
  }

  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>('assets/teacher.json');
  }

  getTeacherById(id: number): Observable<Teacher> {
    return this.http.get<Teacher[]>('assets/teacher.json')
      .pipe(map(teachers => {
        const output: Teacher = (teachers as Teacher[]).find(teacher => teacher.id === +id);
        return output;
      }));
  }

}
