import { TestBed } from '@angular/core/testing';

import { AdminRestImplService } from './admin-rest-impl.service';

describe('AdminRestImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminRestImplService = TestBed.get(AdminRestImplService);
    expect(service).toBeTruthy();
  });
});
