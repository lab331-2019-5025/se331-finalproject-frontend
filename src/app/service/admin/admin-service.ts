import { Observable } from 'rxjs';
import Admin from 'src/app/entity/admin';

export abstract class AdminService {
    abstract getAdmins(): Observable<Admin[]>;
    abstract getAdminById(id: number): Observable<Admin>;
}
