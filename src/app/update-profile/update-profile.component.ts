import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { StudentService } from 'src/app/service/student/student-service';
import Student from 'src/app/entity/student';
import { HttpEventType, HttpEvent } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FileUploadService } from '../service/file-upload.service';
import { MustMatch } from './matchPassword/must-match.validator';
import { AuthenticationService } from '../service/authentication.service';
import { AdminService } from '../service/admin/admin-service';
import { TeacherService } from '../service/teacher/teacher-service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})

export class UpdateProfileComponent implements OnInit {
  defaultImageUrl = 'assets/images/login.png';

  newPassdirty: boolean = false;
  uploadEndPoint: string;
  progress: any;
  uploadedUrl: string;
  confirmPass: string;
  matcher = new MustMatch();

  form = this.fb.group({
    oldPassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(24)])],
    newPassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(24)])],
    confirmPassword: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(24)])]
  }, {
    validator: this.checkPasswords
  });

  validation_messages = {
    'oldPassword': [
      { type: 'minlength', message: 'The password should have at least 6 password' },
      { type: 'maxlength', message: 'The password not more than 24 password' }
    ],
    'newPassword': [
      { type: 'minlength', message: 'The password should have at least 6 password' },
      { type: 'maxlength', message: 'The password not more than 24 password' }
    ]
  };

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private studentService: StudentService,
    private adminService: AdminService,
    private teacherService: TeacherService,
    private router: Router,
    private fileUploadService: FileUploadService,
    private authService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.uploadEndPoint = environment.uploadApi;
  }

  onUploadClicked(files?: FileList) {
    console.log(typeof (files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log('Uploaded! ${this.progress}%');
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            var data = this.fb.group({
              appUserId: this.authService.getCurrentUser().id,
              newPassword: "",
              image: this.uploadedUrl,
              role: this.authService.getCurrentUser().authorities[0].name
            });
            this.authService.updateProfile(data.value)
              .subscribe(user => {
                localStorage.setItem('currentUser', JSON.stringify(user));
                alert("Profile image changed!");
              });

            setTimeout(() => {
              this.progress = 0;
            }, 1500);
        }
      });
  }

  onSelectedFilesChanged(files?: FileList) { }

  get user() {
    return this.authService.getCurrentUser();
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.get('newPassword').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : { notSame: true }
  }

  changePassword() {
    this.authService.checkPassword(this.form.value['oldPassword'])
      .subscribe(userdata => {
        if (userdata.token) {
          var data = this.fb.group({
            appUserId: this.authService.getCurrentUser().id,
            newPassword: this.form.value['newPassword'],
            image: "",
            role: this.authService.getCurrentUser().authorities[0].name
          });
          this.authService.updateProfile(data.value)
            .subscribe(user => {
              localStorage.setItem('currentUser', JSON.stringify(user));
              alert("Passwrod changed!");
              location.reload();
            });
        } else {
          alert("Old password is not correct!");
        }
      },
        error => {
          alert("Old password is not correct!");
        }
      );
  }

}
