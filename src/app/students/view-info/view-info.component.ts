import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import Student from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student/student-service';

@Component({
  selector: 'app-view-info',
  templateUrl: './view-info.component.html',
  styleUrls: ['./view-info.component.css']
})
export class ViewInfoComponent implements OnInit {
  ngOnInit(): void {
    this.route.params
      .subscribe((params: Params) => {
        this.studentService.getStudentById(+params['id'])
          .subscribe((inputStudent: Student) => this.student = inputStudent);
      });

  }
  
  student: Student
  constructor(private route: ActivatedRoute, private studentService: StudentService) { }
  students: Student[];

  getDate(date) {
    var d = new Date(date);
    return d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
  }
}
