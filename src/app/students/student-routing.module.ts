import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewEnrolledActivitiesComponent } from './view-enrolled-activities/view-enrolled-activities.component';
import { StudentsAddComponent } from './add/students.add.component';
import { ViewInfoComponent } from './view-info/view-info.component';
import { StudentGuard } from '../guard/student.guard';
import { AdminAndTeacherAndStudentGuard } from '../guard/admin-and-teacher-and-student.guard';

const StudentRoutes: Routes = [
    { path: 'registration', component: StudentsAddComponent },
    { path: 'view-enrolled-act', component: ViewEnrolledActivitiesComponent, canActivate: [StudentGuard] },
    { path: 'viewStudentinfo/:id', component: ViewInfoComponent, canActivate: [AdminAndTeacherAndStudentGuard] }
];

@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class StudentRoutingModule {

}
