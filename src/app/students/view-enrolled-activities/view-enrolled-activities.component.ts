import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { PendingTableDataSource } from 'src/app/students/view-enrolled-activities/pending-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params } from '@angular/router';
import { ConfirmTableDataSource } from './confirm-table-datasource';
import { StudentService } from 'src/app/service/student/student-service';
import Student from 'src/app/entity/student';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { RejectTableDataSource } from 'src/app/students/view-enrolled-activities/reject-table-datasource';


@Component({
  selector: 'app-view-enrolled-activities',
  templateUrl: './view-enrolled-activities.component.html',
  styleUrls: ['./view-enrolled-activities.component.css']
})
export class ViewEnrolledActivitiesComponent implements OnInit {

  @ViewChild('pendingPaginator', { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('pendingTable', { static: false }) table: MatTable<Activity>;
  dataSourcePending: PendingTableDataSource;

  @ViewChild('confirmPaginator', { static: false }) paginatorC: MatPaginator;
  @ViewChild('confirmTable', { static: false }) tableC: MatTable<Activity>;
  dataSourceConfirm: ConfirmTableDataSource;

  @ViewChild('rejectPaginator', { static: false }) paginatorR: MatPaginator;
  @ViewChild('rejectTable', { static: false }) tableR: MatTable<Activity>;
  dataSourceReject: RejectTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'name', 'host', 'date', 'time'];
  filter: string;
  filter$: BehaviorSubject<string>;
  pendingAct: Activity[];
  confirmAct: Activity[];
  rejectAct: Activity[];
  student: Student;

  constructor(private acivityService: ActivityService, private studentService: StudentService, private route: ActivatedRoute, private authenService: AuthenticationService) { }
  ngOnInit() {
    this.studentService.getStudentById(this.authenService.getCurrentUser().id)
      .subscribe(student => {
        this.student = student;
        var pending = [];
        var confirm = [];
        var reject = [];
        this.acivityService.getActivities()
          .subscribe(activities => {
            for (var i = 0; i < activities.length; i++) {
              for (var j = 0; j < activities[i].studentsEnrolled.length; j++) {
                if (activities[i].studentsEnrolled[j].id == this.authenService.getCurrentUser().id) {
                  confirm.push(activities[i]);
                }
              }
              for (var j = 0; j < activities[i].studentsPending.length; j++) {
                if (activities[i].studentsPending[j].id == this.authenService.getCurrentUser().id) {
                  pending.push(activities[i]);
                }
              }
              for (var j = 0; j < activities[i].studentsReject.length; j++) {
                if (activities[i].studentsReject[j].id == this.authenService.getCurrentUser().id) {
                  reject.push(activities[i]);
                }
              }

              this.pendingAct = pending;
              this.confirmAct = confirm;
              this.rejectAct = reject;
            
            };
            this.dataSourcePending = new PendingTableDataSource();
            this.dataSourcePending.data = this.pendingAct;
            this.dataSourcePending.sort = this.sort;
            this.dataSourcePending.paginator = this.paginator;
            this.table.dataSource = this.dataSourcePending;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSourcePending.filter$ = this.filter$;

            this.dataSourceConfirm = new ConfirmTableDataSource();
            this.dataSourceConfirm.data = this.confirmAct;
            this.dataSourceConfirm.sort = this.sort;
            this.dataSourceConfirm.paginator = this.paginatorC;
            this.tableC.dataSource = this.dataSourceConfirm;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSourceConfirm.filter$ = this.filter$;

            this.dataSourceReject = new RejectTableDataSource();
            this.dataSourceReject.data = this.rejectAct;
            this.dataSourceReject.sort = this.sort;
            this.dataSourceReject.paginator = this.paginatorR;
            this.tableR.dataSource = this.dataSourceReject;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSourceReject.filter$ = this.filter$;
          });
      });
  }


  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  getDate(startDate, endDate) {
    if (startDate == endDate) {
      return this.getDateHelper(startDate);
    } else {
      return this.getDateHelper(startDate) + " - " + this.getDateHelper(endDate);;
    }
  }

  getDateHelper(date) {
    var d = new Date(date);
    return this.dateFormatHelper(d.getDate()) + "/" + this.dateFormatHelper(d.getMonth() + 1) + "/" + d.getFullYear();
  }

  dateFormatHelper(date: number) {
    if (date.toString().length == 1) {
      return "0" + date;
    } else {
      return date;
    }
  }


}
