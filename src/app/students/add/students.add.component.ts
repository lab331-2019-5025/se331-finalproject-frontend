import { Component } from '@angular/core';
import Student from '../../entity/student';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FileUploadService } from 'src/app/service/file-upload.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MustMatch } from './mustMatch/MustMatch';
import { StudentService } from 'src/app/service/student/student-service';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})

export class StudentsAddComponent {
  students: Student[];
  uploadEndPoint: string;
  progress: any;
  uploadedUrl: string;
  confirmPass: string;
  matcher = new MustMatch();

  previewImage = false;

  form = this.fb.group({
    name: [null, Validators.required],
    surname: [null, Validators.required],
    dob: [null, Validators.required],
    studentId: [null, Validators.required],
    major: [null, Validators.required],
    year: [null, Validators.required],
    email: [null, Validators.compose([Validators.required, Validators.email])],
    image: [''],
    username: [''],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(24)])],
    confirmPassword: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(24)])]
  }, { validator: this.checkPasswords });

  ngOnInit(): void {
    this.uploadEndPoint = environment.uploadApi;
  }

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private fileUploadService: FileUploadService,
    private studentService: StudentService
  ) { }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required' }
    ],
    'surname': [
      { type: 'required', message: 'Surame is required' }
    ],
    'dob': [
      { type: 'required', message: 'Date of birthday is required' }
    ],
    'studentId': [
      { type: 'required', message: 'Student ID is required' }
    ],
    'major': [
      { type: 'required', message: 'Major is required' }
    ],
    'year': [
      { type: 'required', message: 'Year is required' }
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Your email is incorrect pattern, example: asd@cm.com' }
    ],
    'password': [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'The password should have at least 6 password' },
      { type: 'maxlength', message: 'The password not more than 24 password' }
    ],
    'confirmPassword': [
      { type: 'required', message: 'Confirm Password is required' },
      { type: 'minlength', message: 'The password should have at least 6 password' },
      { type: 'maxlength', message: 'The password not more than 24 password' }
    ]
  };

  checkPasswords(group: FormGroup) {
    let password = group.get('password').value;
    let confirmPassword = group.get('confirmPassword').value;

    return password === confirmPassword ? null : { notSame: true }
  }

  submit() {
    this.uploadEndPoint = environment.uploadApi;
    var addStuForm = this.fb.group({
      name: this.form.value['name'],
      surname: this.form.value['surname'],
      dob: this.form.value['dob'].toISOString(),
      studentId: this.form.value['studentId'],
      major: this.form.value['major'],
      year: this.form.value['year'],
      email: this.form.value['email'],
      image: this.form.value['image'],
      username: this.form.value['username'],
      password: this.form.value['password'],
      confirmPassword: this.form.value['confirmPassword']
    });
    this.studentService.saveStudent(addStuForm.value)
      .subscribe((student) => {
        this.router.navigate(['/login']);
        alert("Registration complete");
      }, (error) => {
        alert('could not save value');
      });
  }

  onUploadClicked(files?: FileList) {
    console.log(typeof (files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.form.patchValue({
              image: this.uploadedUrl
            });
            this.form.get('image').updateValueAndValidity();
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
        }
      });
  }

  onSelectedFilesChanged(files?: FileList) {
    console.log(files);
    const objectURL = window.URL.createObjectURL(files.item(0));
    console.log(window.URL.revokeObjectURL(objectURL));

    var preview = document.getElementById('imgPre');
    preview.innerHTML = "";

    const img = <HTMLImageElement>document.createElement("img");
    img.classList.add("img-width");
    img.setAttribute('style', 'max-width: 350px; padding: 30px 0px 10px 0px;');
    preview.appendChild(img);

    const reader = new FileReader();
    reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
    reader.readAsDataURL(files.item(0));
  }

  showUsername() {
    this.form.patchValue({
      username: this.form.value['email']
    });
  }

  imageUploaded() {
    if (this.form.value['image'] == "") {
      return false;
    } else {
      return true;
    }
  }

}
