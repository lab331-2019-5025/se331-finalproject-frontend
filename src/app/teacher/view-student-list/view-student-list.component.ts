import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { StudentTableDataSource } from './student-table-datasource';
import Student from 'src/app/entity/student';
import { BehaviorSubject, from } from 'rxjs';
import { StudentService } from 'src/app/service/student/student-service';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params } from '@angular/router';
import Activity from 'src/app/entity/activity';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-view-student-list',
  templateUrl: './view-student-list.component.html',
  styleUrls: ['./view-student-list.component.css']
})

export class ViewWaitingStudentComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['studentId', 'name', 'surname', 'major', 'year', 'btnCol'];
  students: Student[];
  activity: Activity;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private studentService: StudentService, private acivityService: ActivityService, private fb: FormBuilder) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.acivityService.getActivityById(params['actId'])
          .subscribe(activity => {
            this.activity = activity;
            console.log(this.activity);
            this.students = activity.studentsPending;
            console.log(this.students);
            this.dataSource = new StudentTableDataSource();
            this.dataSource.data = this.students;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = this.dataSource;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSource.filter$ = this.filter$;
          });
      });
  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  get getActName() {
    return this.students;
  }

  confirm(student: Student) {
    var form = this.fb.group({
      studentId: student.id,
      activityId: this.activity.id
    });
    this.acivityService.confirmEnrollActivity(form.value)
      .subscribe((form) => {
        alert(student.studentId + " " + student.name + " confirmed!");
        this.ngOnInit();
      }, (error) => {
        alert('could not confirm');
      });
  }

  reject(student: Student) {
    var form = this.fb.group({
      studentId: student.id,
      activityId: this.activity.id
    });
    this.acivityService.rejectEnrollActivity(form.value)
      .subscribe((form) => {
        alert(student.id + " " + student.name + " rejected!");
        this.ngOnInit();
      }, (error) => {
        alert('could not reject');
      });
  }

}
