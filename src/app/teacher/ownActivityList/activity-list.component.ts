import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { TeacherService } from 'src/app/service/teacher/teacher-service';
import Teacher from 'src/app/entity/teacher';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class TeacherOwnActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'name', 'date', 'time', 'location', 'periodRegis', 'btnCol'];
  activities: Activity[];
  teacher: Teacher;
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private acivityService: ActivityService, private teacherService: TeacherService, private authService: AuthenticationService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.teacherService.getTeacherById(this.authService.getCurrentUser().id)
          .subscribe(teacher => {
            this.teacher = teacher;
            this.activities = teacher.activities;
            console.log(this.activities);
            this.dataSource = new ActivityTableDataSource();
            this.dataSource.data = this.activities;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = this.dataSource;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSource.filter$ = this.filter$;
          });
        });
      }

  getDate(startDate, endDate) {
        if(startDate == endDate) {
      return this.getDateHelper(startDate);
    } else {
      return this.getDateHelper(startDate) + " - " + this.getDateHelper(endDate);;
    }
  }

  getDateHelper(date) {
    var d = new Date(date);
    return this.dateFormatHelper(d.getDate()) + "/" + this.dateFormatHelper(d.getMonth() + 1) + "/" + d.getFullYear();
  }

  dateFormatHelper(date: number) {
    if (date.toString().length == 1) {
      return "0" + date;
    } else {
      return date;
    }
  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  remove(activity: Activity) {
    console.log("Remove activity id " + activity.id);
  }

}
