import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { WaitingListComponent } from './waiting-list/waiting-list.component';
import { ViewWaitingStudentComponent } from './view-student-list/view-student-list.component';
import { TeacherOwnActivityListComponent } from './ownActivityList/activity-list.component';
import { ViewEnrolledStudentComponent } from './view-enrolled-student/view-enrolled-student.component';
import { TeacherGuard } from '../guard/teacher.guard';

const TeacherRoutes: Routes = [
    { path: 'ownActivityList', component: TeacherOwnActivityListComponent, canActivate: [TeacherGuard] },
    { path: 'viewActivityEnrolledStudent/:actId', component: ViewEnrolledStudentComponent, canActivate: [TeacherGuard] },
    { path: 'viewWaitingStudent/:actId', component: ViewWaitingStudentComponent, canActivate: [TeacherGuard] },
    { path: 'chooseActToViewWaitingStudent', component: WaitingListComponent, canActivate: [TeacherGuard] }
];

@NgModule({
    imports: [
        RouterModule.forRoot(TeacherRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TeacherRoutingModule {

}
