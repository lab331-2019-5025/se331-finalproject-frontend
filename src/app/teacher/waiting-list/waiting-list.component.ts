import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { ActivatedRoute, Params } from '@angular/router';
import { TeacherService } from 'src/app/service/teacher/teacher-service';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-waiting-list',
  templateUrl: './waiting-list.component.html',
  styleUrls: ['./waiting-list.component.css']
})

export class WaitingListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'name', 'date', 'time', 'numOfStuInList',  'viewBtn'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private teacherService: TeacherService, private authService: AuthenticationService) { }

  ngOnInit() {
    this.teacherService.getTeacherById(this.authService.getCurrentUser().id)
      .subscribe(teacher => {
        this.activities = teacher.activities;
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = this.activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;

        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      });

  }

  ngAfterViewInit() { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  getDate(startDate, endDate) {
    if (startDate == endDate) {
      return this.getDateHelper(startDate);
    } else {
      return this.getDateHelper(startDate) + " - " + this.getDateHelper(endDate);;
    }
  }

  getDateHelper(date) {
    var d = new Date(date);
    return this.dateFormatHelper(d.getDate()) + "/" + this.dateFormatHelper(d.getMonth() + 1) + "/" + d.getFullYear();
  }

  dateFormatHelper(date: number) {
    if (date.toString().length == 1) {
      return "0" + date;
    } else {
      return date;
    }
  }

}
