import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ViewEnrolledStudentDataSource } from './view-enrolled-student-datasource';
import Student from 'src/app/entity/student';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from 'src/app/service/student/student-service';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { BehaviorSubject } from 'rxjs';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-view-enrolled-student',
  templateUrl: './view-enrolled-student.component.html',
  styleUrls: ['./view-enrolled-student.component.css']
})
export class ViewEnrolledStudentComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: ViewEnrolledStudentDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['studentId', 'name', 'surname', 'major', 'year'];
  activity: Activity;
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private studentService: StudentService, private activityService: ActivityService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivityById(params['actId'])
          .subscribe(activity => {
            this.activity = activity;
            console.log(this.activity);
            this.students = activity.studentsEnrolled;
            this.dataSource = new ViewEnrolledStudentDataSource();
            this.dataSource.data = this.students;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.table.dataSource = this.dataSource;
            this.filter$ = new BehaviorSubject<string>('');
            this.dataSource.filter$ = this.filter$;
            console.log(this.students);
          });
      });
  }

  ngAfterViewInit(): void { }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
