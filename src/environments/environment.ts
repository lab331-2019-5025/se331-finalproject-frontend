// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  uploadApi: 'http://localhost:8080/uploadFile',
  activityApi: 'http://localhost:8080/activities',
  adminApi: 'http://localhost:8080/admins',
  commentApi: 'http://localhost:8080/comments',
  studentApi: 'http://localhost:8080/students',
  teacherApi: 'http://localhost:8080/teachers',
  updateActivityApi: 'http://localhost:8080/updateActivity',
  enrollActivityApi: 'http://localhost:8080/enrollActivity',
  confirmEnrollActivityApi: 'http://localhost:8080/confirmEnrollActivity',
  rejectEnrollActivityApi: 'http://localhost:8080/rejectEnrollActivity',
  rejectRegisStudentApi: 'http://localhost:8080/rejectRegisStudent',
  confirmRegisStudentApi: 'http://localhost:8080/confirmRegisStudent',
  authenticationApi: 'http://localhost:8080/auth',
  removeCommentApi: 'http://localhost:8080/removeComment',
  updateProfileApi: 'http://localhost:8080/updateProfile'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
